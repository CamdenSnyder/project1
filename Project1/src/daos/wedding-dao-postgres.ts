import { Wedding } from "../entities";
import { WeddingDAO } from "./wedding-dao";
import { client } from "../connection";
import { MissingResourceError } from "../errors";




export class WeddingDaoPostgres implements WeddingDAO{

    async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = "insert into wedding (w_date, w_location, w_name, w_budget) values ($1,$2,$3,$4) returning w_id";
        const values = [wedding.wDate, wedding.wLocation, wedding.wName, wedding.wBudget];
        const result = await client.query(sql, values);
        wedding.wId = result.rows[0].w_id;
        return wedding;
    }

    async getAllWeddings(): Promise<Wedding[]> {
        const sql:string = 'select * from wedding';
        const result = await client.query(sql);
        const weddings:Wedding[] = [];
        for(const row of result.rows){
            const wedding:Wedding = new Wedding(
                row.w_id,
                row.w_date,
                row.w_location,
                row.w_name,
                row.w_budget);
            weddings.push(wedding);
        }
        return weddings;
    }

    async getWeddingById(weddingId: number): Promise<Wedding> {
        const sql:string = 'select * from wedding where w_id = $1';
        const values = [weddingId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${weddingId} does not exist`);
        }
        const row = result.rows[0];
        const wedding:Wedding = new Wedding(
                row.w_id,
                row.w_date,
                row.w_location,
                row.w_name,
                row.w_budget);
        return wedding;
    }

    async updateWedding(wedding:Wedding, weddingId:number): Promise<Wedding> {
        const sql:string = 'update wedding set w_date = $1, w_location = $2, w_name = $3, w_budget = $4 where w_id = $5'
        const values = [wedding.wDate, wedding.wLocation, wedding.wName, wedding.wBudget, weddingId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${wedding.wId} does not exist`);
        }
        return wedding;
    }

    async deleteWeddingById(weddingId: number): Promise<boolean> {
        const sql:string = 'delete from wedding where w_id = $1';
        const values = [weddingId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${weddingId} does not exist`);
        }
        return true;
    }
    
}