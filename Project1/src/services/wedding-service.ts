import { Wedding } from "../entities";


export default interface WeddingService{

    registerWedding(wedding:Wedding):Promise<Wedding>;

    retrieveAllWeddings():Promise<Wedding[]>;

    retrieveWeddingById(weddingId:number):Promise<Wedding>;

    modifyWedding(wedding:Wedding, weddingId:number):Promise<Wedding>;

    removeWeddingById(weddingId:number):Promise<boolean>;
}