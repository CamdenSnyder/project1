import { WeddingDAO } from "../daos/wedding-dao";
import { WeddingDaoPostgres } from "../daos/wedding-dao-postgres";
import { Wedding } from "../entities";
import WeddingService from "./wedding-service";




export class WeddingServiceImpl implements WeddingService{

    weddingDAO:WeddingDAO = new WeddingDaoPostgres();

    registerWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.createWedding(wedding);
    }

    retrieveAllWeddings(): Promise<Wedding[]> {
        return this.weddingDAO.getAllWeddings();
    }

    retrieveWeddingById(weddingId: number): Promise<Wedding> {
        return this.weddingDAO.getWeddingById(weddingId);
    }

    modifyWedding(wedding: Wedding, weddingId:number): Promise<Wedding> {
        return this.weddingDAO.updateWedding(wedding, weddingId);
    }

    removeWeddingById(weddingId: number): Promise<boolean> {
        return this.weddingDAO.deleteWeddingById(weddingId);
    }
    
}