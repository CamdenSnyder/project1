import { Expense } from "../entities";



export default interface ExpenseService{

    registerExpense(expense:Expense):Promise<Expense>;

    retrieveAllExpenses():Promise<Expense[]>;

    retrieveExpensesByWid(wId:number):Promise<Expense[]>;

    retrieveExpenseById(expenseId:number):Promise<Expense>;

    modifyExpense(expense:Expense):Promise<Expense>;

    removeExpenseById(expenseId:number):Promise<boolean>;
}