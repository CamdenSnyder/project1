# Wedding Planner

## Project Description
This application is for users to manage weddings. It provides the functionality to add, edit, or delete both weddings and expenses. Additionally, it provides a messaging service for users to comminicate with each other.

## Technologies Used
- Node.js 14
- React
- Cloud SQL
- GCP Datastore
- GCP App Engine
- Firebase

## Features
List of features ready and TODOs for future development
- Wedding planner service using Cloud SQL and Compute Engine
- Authentication service implemented for validating and logging in users
- A messaging service for sending and storing messages between users using Datastore

To-do list:
- Fully implement File Upload functionality

## Getting Started
- Run in bash "git clone https://gitlab.com/CamdenSnyder/project1.git
- In the command prompt, cd to the WedFrontend directory and run "npm install"
- Finally, run "npm start" to host the app locally

## Usage

- Click on the "Login Page" hyperlink to arrive here where you can log in to unlock certain functionalities on the Wedding Page
![Login Page](/Not_Logged_In.PNG)

- Now go to the Wedding Page
![Wedding Planner](/Wedding_Planner.PNG)
- Here you can click "Show Weddings" to view existing ones
- You can enter the related info and create a new one
- Using the same inputs, you can also supply a wedding ID to replace an existing wedding.
- Finally you can supply a wedding ID to delete a wedding

- Now go to the Expense Page
![Expense Planner](/Expense_Planner.PNG)
- This page is very similar to the wedding page, but is for expenses instead
- Here you can show expenses as well as create, replace, and delete them
- Additionally, you can select a file to upload with the expense for related images, but the file upload feature has not yet been implemented.

- Finally move to the Messages Page
![Messages](Messages.PNG)
- In this page you can filter any messages in the database by the sender, the recipient, or both using the first to inputs.
- You can also use the bottom three inputs to send a new message that will be stored in Datastore