import { Datastore } from '@google-cloud/datastore';
import express from 'express';
import cors from 'cors';

const datastore = new Datastore();

const app = express()
app.use(express.json());
app.use(cors())

app.get("/messages/:mid", async (req,res)=>{
    const key = datastore.key(['message',Number(req.params.mid)])
    const response = await datastore.get(key);
    res.send(response);
})

app.get("/messages", async (req,res)=>{
    if(req.query.recipient && req.query.sender){
        const query = datastore.createQuery('message').filter('recipient', '=', req.query.recipient).filter('sender','=',req.query.sender);
        const [data, metaInfo] = await datastore.runQuery(query);
        if(data[0]){
            res.send(data);
            res.status(200)
        }else{
            res.send("Message(s) not found")
            res.status(404);
        }
    }else if(req.query.recipient){
        const query = datastore.createQuery('message').filter('recipient', '=', req.query.recipient);
        const [data, metaInfo] = await datastore.runQuery(query);
        if(data[0]){
            res.send(data);
            res.status(200)
        }else{
            res.send("Message(s) not found")
            res.status(404);
        }
    }else if(req.query.sender){
        const query = datastore.createQuery('message').filter('sender', '=', req.query.sender);
        const [data, metaInfo] = await datastore.runQuery(query);
        if(data[0]){
            res.send(data);
            res.status(200)
        }else{
            res.send("Message(s) not found")
            res.status(404);
        }
    }else{
        const query = datastore.createQuery('message');
        const [data, metaInfo] = await datastore.runQuery(query);
        if(data[0]){
            res.send(data);
            res.status(200);
        }else{
            res.send("Message(s) not found");
            res.status(404);
        }
    }
})

app.post("/messages", async(req,res)=>{
    const message = req.body;
    const key = datastore.key(['message']);
    const response = await datastore.save({key:key, data:message});
    res.status(201);
    res.send('Successfully sent message')
})



const PORT = process.env.PORT || 3001;

app.listen(PORT, ()=>{console.log("Application Started")})