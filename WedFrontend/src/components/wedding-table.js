

export default function WeddingTable(props){

    const weddings = props.weddings;

    return(<table className='table'>
        <thead><tr><th>ID</th><th>Date</th><th>Location</th><th>Name</th><th>Budget</th></tr></thead>
        <tbody style={{textAlign: "center"}}>
            {weddings.map(w => <tr key={w.wId}>
                <td>{w.wId}</td>
                <td>{w.wDate}</td>
                <td>{w.wLocation}</td>
                <td>{w.wName}</td>
                <td>{w.wBudget}</td>
            </tr>)}
        </tbody>
    </table>)
}