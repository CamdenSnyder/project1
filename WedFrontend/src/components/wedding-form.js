import axios from "axios";
import { SyntheticEvent, useState } from "react";
import {useRef} from "react";
import WeddingTable from "./wedding-table";
import { Card } from "react-bootstrap";

export default function WeddingForm(){

    const [weddings,setWeddings] = useState([]);
    const [expenses,setExpenses] = useState([]);

    const dateInput = useRef(null);
    const locationInput = useRef(null);
    const nameInput = useRef(null);
    const budgetInput = useRef(null);
    const idInput = useRef(null);

    const deleteWedIn = useRef(null);
    
    
    // Weddings
    async function getWeddings(event){
        const response = await axios.get('http://35.227.125.183:3000/weddings');
        setWeddings(response.data);
    }

    async function postWedding(event){
        const date = dateInput.current.value;
        const location = locationInput.current.value;
        const name = nameInput.current.value;
        const budget = budgetInput.current.value;
        const newWedding = {wId:0, wDate:date, wLocation:location, wName:name, wBudget:budget}
        const response = await axios.post('http://35.227.125.183:3000/weddings', newWedding);
    }

    async function putWedding(event){
        const date = dateInput.current.value;
        const location = locationInput.current.value;
        const name = nameInput.current.value;
        const budget = budgetInput.current.value;
        const id = idInput.current.value;
        const newWedding = {wId:0, wDate:date, wLocation:location, wName:name, wBudget:budget}
        const response = await axios.put(`http://35.227.125.183:3000/weddings/${id}`, newWedding);
    }

    async function deleteWedding(event){
        if(deleteWedIn){
            const wedID = deleteWedIn.current.value;
            const response = await axios.delete(`http://35.227.125.183:3000/weddings/${wedID}`);
        }else{
            alert("Must provide input")
        }
    }


    return(<div style={{textAlign:"center"}}>

        <h1>Wedding Planner</h1>
        <button onClick={getWeddings}>Show Weddings</button>
        <WeddingTable weddings={weddings} style={{textAlign: "center"}}></WeddingTable>
        <h2>Create a New Wedding</h2>
        <input placeholder="date" ref={dateInput}></input>
        <input placeholder="location" ref={locationInput}></input>
        <input placeholder="name" ref={nameInput}></input>
        <input placeholder="budget" ref={budgetInput}></input>
        <button onClick={postWedding}>Create Wedding</button>
        <div>
            <h3>Replace an existing wedding</h3>
            <input placeholder="id" ref={idInput}></input>
            <button onClick={putWedding}>Replace</button>
        </div>
        <h2>Delete a Wedding</h2>
        <input placeholder="Wedding ID" ref={deleteWedIn}></input>
        <button onClick={deleteWedding}>Delete Wedding</button>

        
    </div>)

}